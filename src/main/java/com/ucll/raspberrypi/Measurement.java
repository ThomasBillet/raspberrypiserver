/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucll.raspberrypi;

/**
 *
 * @author thomas
 */
public class Measurement {
    
    private double temp;
    private double humidity;

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public Measurement(double temp, double humidity) {
        this.temp = temp;
        this.humidity = humidity;
    }

    public Measurement() {
    
    
    }
    
    
    
}
