/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucll.raspberrypi.rest;

import com.ucll.raspberrypi.Humidity;
import com.ucll.raspberrypi.Model;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author thomas
 */



 


 
@Path("/humidity")
public class HumidityRestService {
    
    @Inject
    private Model model;
    
    
    @GET
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    @Secure
    public Humidity getHumidity(){
        
        return new Humidity(this.model.getMeasurement().getHumidity());
         
        
    }
    
    
}
