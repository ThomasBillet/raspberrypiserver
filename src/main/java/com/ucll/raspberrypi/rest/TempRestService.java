/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucll.raspberrypi.rest;

import com.ucll.raspberrypi.Model;
import com.ucll.raspberrypi.Temperature;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.NameBinding;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author thomas
 */




@Path("/temperature")
public class TempRestService {
    
    @Inject
    private Model model;
    
    
    @GET
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    @Secure
    public Temperature getTemprature(){
        return new Temperature(this.model.getMeasurement().getTemp());
        
    }
    
}
