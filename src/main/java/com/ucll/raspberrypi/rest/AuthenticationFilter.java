/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucll.raspberrypi.rest;

import com.ucll.raspberrypi.AutoRizer;
import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import org.glassfish.jersey.internal.util.Base64;

/**
 *
 * @author thomas
 */



@Secure
@Provider
public class AuthenticationFilter implements ContainerRequestFilter {
    
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if(requestContext.getHeaderString("Authorization") != null){
        String user = requestContext.getHeaderString("Authorization");
        
        String[] combo = user.split(" ");
        String decoded = Base64.decodeAsString(combo[1]);
        String[] combo1 = decoded.split(":");
        
        AutoRizer autorizer = new AutoRizer();
        if(!autorizer.autorize(combo1[0], combo1[1])){
                requestContext.abortWith(Response
                    .status(Response.Status.UNAUTHORIZED)
                    .entity("User cannot access the resource.")
                    .build());
        }
    }
    
else{
    
    requestContext.abortWith(Response
                    .status(Response.Status.UNAUTHORIZED)
                    .entity("User cannot access the resource.")
                    .build());
}
    
    }
}
    

    

