package com.ucll.raspberrypi;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author thomas
 */
import java.io.*;
import javax.enterprise.inject.Default;


@Default
public class PythonExecutor implements Executor{

    public PythonExecutor() {
   
    
    }
    
 @Override
public String execute(){
    
     try {

        //Process p = Runtime.getRuntime().exec("python /home/pythonscript/script.py
        Process p = Runtime.getRuntime().exec("python /home/pythonscript/script.py");
        BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String ret = in.readLine();
        System.out.println(ret);
         // "{temp:18.6,humidity:48.23}";
         return ret;
     } catch (Exception e) {
         throw new RuntimeException(e);
     }

/*
    try{
    final ProcessBuilder builder = new ProcessBuilder("python C:/Users/thomas/Desktop/python/test1.py")
    .redirectErrorStream(true);
    final Process p = builder.start();

    BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
    String ret = in.readLine();
    System.out.println(ret);
    return ret;
    }catch(Exception e){
        throw new RuntimeException(e);
    }
    */
}


}
