/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucll.raspberrypi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.enterprise.inject.Default;

/**
 *
 * @author thomas
 */
@Default
public class Model {
    
    private Executor executor = new PythonExecutor();
    
    public Measurement getMeasurement()
    {
       Gson gson = new GsonBuilder().create();
       String json = executor.execute();
       Measurement meas = gson.fromJson(json, Measurement.class);
       return meas;
    }
    
}
