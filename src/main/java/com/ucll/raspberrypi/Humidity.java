/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucll.raspberrypi;

/**
 *
 * @author thomas
 */
public class Humidity {
    private double humidity;

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public Humidity() {
    }

    public Humidity(double humidity) {
        this.humidity = humidity;
    }

    @Override
    public String toString() {
        return "Humidity{" + "humidity=" + humidity + '}';
    }
    
    
    
    
}
