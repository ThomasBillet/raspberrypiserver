/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ucll.raspberrypi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author thomas
 */
public class AutoRizer {
    
    private Map<String,String> users = new HashMap<String,String>();

    public AutoRizer() {
    try{
           ClassLoader classLoader = getClass().getClassLoader();
           
                BufferedReader reader = new BufferedReader(new FileReader(new File(classLoader.getResource("users.txt").getFile())));

      while(reader.ready()){
          String line = reader.readLine();
          
          String[] split1 = line.split(":");
          users.put(split1[0], split1[1]);
          
      }
      
        }catch(Exception e ){
                       
        }
    
    }
    
    
    
    
    public boolean autorize(String user,String password){
        boolean bol = users.containsKey(user) && users.get(user).equals(password);    
        return bol;
         
    }
    
}
