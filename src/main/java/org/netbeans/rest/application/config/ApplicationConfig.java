/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.rest.application.config;

import com.ucll.raspberrypi.rest.AuthenticationFilter;
import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author thomas
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        
        addRestResourceClasses(resources);
        resources.add(AuthenticationFilter.class);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.ucll.raspberrypi.rest.AuthenticationFilter.class);
        resources.add(com.ucll.raspberrypi.rest.HumidityRestService.class);
        resources.add(com.ucll.raspberrypi.rest.TempRestService.class);
    }
    
}
